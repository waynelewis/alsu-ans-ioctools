alsu-ans-ioctools
=================

Ansible playbook to install IOC server tools

Variables
---------

Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A script is provided to easily provision a machine.
It will install ansible locally.
This is for testing purpose and not recommended for production.

To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.4.0.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://gitlab.com/waynelewis/alsu-ans-ioctools.git
    $ cd alsu-ans-ioctools
    # Edit the "hosts" file
    $ make playbook


Testing
-------

No testing configured at the moment.

License
-------

BSD 2-clause
